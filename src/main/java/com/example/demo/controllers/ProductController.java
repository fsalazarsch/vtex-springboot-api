package com.example.demo.controllers;

import com.example.demo.models.Producto;
import com.example.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    private ProductService prodService;

    @GetMapping("/")
    public String test(){
        return "API Base";
    }



    @GetMapping("/productos")
    public List<Producto> findAll(){
        return prodService.findAll();
    }


    @GetMapping("/productos/{id}")
    public Optional<Producto> getPrdo(@PathVariable int id){
        Optional<Producto> prod = prodService.findById(id);

        if(prod == null) {
            throw new RuntimeException("No existe el id: "+id);
        }
        return prod;
    }


    @PostMapping("/prodcto")
    public Producto addProd(@RequestBody Producto prod) {
        prod.setId(0);
        prodService.save(prod);
        return prod;

    }


    @PutMapping("/producto")
    public Producto updateProd(@RequestBody Producto prod) {
        prodService.save(prod);
        return prod;
    }


    @DeleteMapping("producto/{id}")
    public String deteteProd(@PathVariable int id) {

        Optional<Producto> prod = prodService.findById(id);

        if(prod == null) {
            throw new RuntimeException("No existe el id: "+id);
        }
        prodService.deleteById(id);
        return "Producto "+id+ " borrado";
    }

}
