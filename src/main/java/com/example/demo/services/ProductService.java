package com.example.demo.services;

import com.example.demo.dao.ProductRepository;
import com.example.demo.models.Producto;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class ProductService {

    @Autowired
    private ProductRepository productRepository;


    public List<Producto> findAll() {
        List<Producto> listPOrod= (List<Producto>) productRepository.findAll();
        return listPOrod;
    }


    public Optional<Producto> findById(int id) {
        Optional<Producto> producto = productRepository.findById(id);
        return producto;
    }


    public void save(Producto producto) {
        productRepository.save(producto);

    }

    public void deleteById(int id) {
        productRepository.deleteById(id);
    }
}
