package com.example.demo.dao;

import com.example.demo.models.Producto;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Producto, Integer> {
    //@Query(value = "Select * from producto");
    //public List<String> buscador();
}
